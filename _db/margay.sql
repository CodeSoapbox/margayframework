-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 04, 2016 at 03:25 PM
-- Server version: 5.6.14
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `margay`
--

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` bigint(20) unsigned NOT NULL,
  `resource` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `resource`) VALUES
(1, 'modules/base/access'),
(2, 'modules/base/routes/home/index'),
(3, 'modules/base/routes/home/textclicked'),
(4, 'modules/base/routes/about/index'),
(5, 'modules/base/routes/post/view'),
(6, 'modules/admin/access'),
(7, 'modules/admin/routes/home/index'),
(8, 'modules/admin/routes/posts/index'),
(9, 'modules/admin/routes/settings/index'),
(10, 'modules/admin/routes/users/index'),
(11, 'modules/admin/routes/login/index'),
(13, 'modules/base/routes/error/pagenotfound'),
(14, 'modules/admin/routes/logout/index');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `user_id_fk` bigint(20) NOT NULL COMMENT 'ID of the user that created the post',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `image`, `description`, `user_id_fk`, `date_created`, `date_modified`) VALUES
(1, 'Some post', 'https://farm6.staticflickr.com/5512/14067533595_cac05d3cc8.jpg', 'A description is here', 1, '2016-07-01 16:13:32', '2016-07-01 16:44:13'),
(2, 'Some other post', 'https://farm6.staticflickr.com/5524/11900242224_8d4aa77d7a.jpg', 'And another description.', 1, '2016-07-01 16:13:47', '2016-07-01 16:44:30');

-- --------------------------------------------------------

--
-- Table structure for table `role_grants`
--

CREATE TABLE IF NOT EXISTS `role_grants` (
  `role_id_fk` int(20) unsigned NOT NULL,
  `permission_id_fk` bigint(20) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_grants`
--

INSERT INTO `role_grants` (`role_id_fk`, `permission_id_fk`) VALUES
(1, 1),
(2, 1),
(1, 2),
(2, 2),
(1, 3),
(2, 3),
(1, 4),
(2, 4),
(1, 5),
(2, 5),
(1, 6),
(2, 6),
(2, 7),
(2, 8),
(2, 9),
(2, 10),
(1, 11),
(2, 11),
(1, 13),
(2, 13),
(1, 14),
(2, 14);

-- --------------------------------------------------------

--
-- Table structure for table `role_members`
--

CREATE TABLE IF NOT EXISTS `role_members` (
  `user_id_fk` bigint(20) unsigned NOT NULL,
  `user_role_id_fk` int(20) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_members`
--

INSERT INTO `role_members` (`user_id_fk`, `user_role_id_fk`) VALUES
(1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL,
  `login` varchar(128) NOT NULL,
  `pass` varchar(128) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `name` varchar(250) NOT NULL COMMENT 'Display name',
  `date_registered` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `login`, `pass`, `mail`, `name`, `date_registered`) VALUES
(1, 'admin', '$2y$10$oGlUXUJo7wgjkD5Uh3rIBuWcI.cRC098XM4zZrDTuyKRHqwzkIIMC', 'admin@admin.com', 'John', '2016-07-01 16:12:43');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
  `id` int(20) unsigned NOT NULL,
  `title` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `title`) VALUES
(2, 'Administrator'),
(1, 'Guest');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_grants`
--
ALTER TABLE `role_grants`
  ADD UNIQUE KEY `role_permission_unique` (`role_id_fk`,`permission_id_fk`),
  ADD KEY `permission_id_fk` (`permission_id_fk`) USING BTREE;

--
-- Indexes for table `role_members`
--
ALTER TABLE `role_members`
  ADD UNIQUE KEY `user_user_role_unique` (`user_id_fk`,`user_role_id_fk`),
  ADD KEY `user_id_fk` (`user_id_fk`) USING BTREE,
  ADD KEY `user_role_id_fk` (`user_role_id_fk`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`),
  ADD UNIQUE KEY `mail` (`mail`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `role_grants`
--
ALTER TABLE `role_grants`
  ADD CONSTRAINT `role_grants_ibfk_1` FOREIGN KEY (`role_id_fk`) REFERENCES `user_roles` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `role_grants_ibfk_2` FOREIGN KEY (`permission_id_fk`) REFERENCES `permissions` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `role_members`
--
ALTER TABLE `role_members`
  ADD CONSTRAINT `role_members_ibfk_1` FOREIGN KEY (`user_id_fk`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `role_members_ibfk_2` FOREIGN KEY (`user_role_id_fk`) REFERENCES `user_roles` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
