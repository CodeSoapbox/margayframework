# README #

## Margay CMS ##

### What is Margay CMS? ###

* Margay CMS is a micro CMS for photoblogs written as an experiment in writing proper code. It is a work in progress, ever-expanding for purposes of personal edification.

### Who wrote Margay CMS? ###

* Margay CMS was written by Daniel Frąk. You can contact me at: <dan.frak@gmail.com>.

### Some notes on how Margay works ###

* The core of Margay exists within the \Margay namespace. The MVC structure for the administration backend exists within its own module and uses the namespace \Margay\Admin, while the frontend resides within the 'base' module and uses the namespace \Margay\Base. The models, views and controllers all have their own corresponding folders within their modules' directories. It's possible to create more modules which can be registered using the addModule() method of the Router object.
* Most of the MVC grunt work is done in an instance of the FrontController class, which takes routing information from an instance of the Router class and creates the necessary models, views and controllers, which in turn take on their corresponding roles.
* Margay uses a concept described as a ViewModels by Tom Butler in his article "Model-View-Confusion part 2: MVC models are not domain models" (<https://r.je/view-helpers.html>) or View Helpers by Padraic Brady, from whom he has taken the concept. In summary, this means separating the domain model from the view using a separate "helper" class which "contains all the interaction between the model and the view". This solves the problem of cluttering the model with things not related to its domain. The distinction I have decided to use in this project is as follows: Domain models are called by the names of the things they represent (e.g. Post), while ViewModels/View helpers have the term "Model" suffixed to their names (e.g. PostModel).

### Additional notes and random thoughts ###

* I started working on Margay as a means to further my knowledge of writing proper code. It was decided early on that common anti-patterns (such as service locators and an overuse of singletons) will be ignored in favor of finding proper solutions to problems presented to the everyday programmer. The code that makes up this project is highly experimental and is intended to evolve with my own understanding of the proper use of design patterns.

* Through my research I have found that the programming community is divided on the subject of the usefulness of many patterns and whether or not they should be used by professionals. Among the debated ideas are singletons, which some say are the spawn of the devil, while others defend them claiming that they have their uses. This is why I have decided to use a singleton pattern for the logging system and also why some code may not be fully "correct" in the eyes of programming purists. Furthermore, some parts of the code may differ from others, as I'm using this project as a means to discover and compare ideas, rather than an actual product to be distributed to clients.

* Because my aim is to learn as much as possible from this project, and to avoid bloating the microCMS, I have decided to not use any existing frameworks or ready-made solutions and have opted for writing Margay using pure PHP. I am aware that there exist great solutions to many of the problems I'm solving here and that, many times, I'm reinventing the wheel. Reinventing the wheel, however, is often a good learning experience.