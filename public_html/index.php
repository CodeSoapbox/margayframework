<?php
/**
 * @author Daniel Frąk <dan.frak@gmail.com>
 * @link https://www.linkedin.com/in/danielfrak
 */

namespace Margay;

//Start the session
session_start();

//Include configuration files and basic functionality
require_once 'config.php';
require_once 'includes/functions.php';

//Create router
$router = new Router($autoload_func);
$router->addModule('admin');

//Create the database adapter
$database = (new Database())
        ->setHost(DB_HOST)
        ->setUser(DB_USER)
        ->setPass(DB_PASS)
        ->setDbName(DB_NAME);

//Create user
$userManager = new UserManager($database);
$user = $userManager->getUser();

//Create ACL
//$acl = new DummyACL($database);
$acl = new ACL($database);
$acl->setRedirectURL('/admin/login');
if(isset($user)){
    $acl->setUser($user);
}

//Initialize Front Controller and render the page
$frontController = new FrontController($router, $database, $userManager, $user, $acl);
echo $frontController->output();
?>