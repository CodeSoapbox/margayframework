<?php

/**
 * Interface for a storable object
 */

namespace Margay;

interface iStorable {
    public function load();
    public function save();
}
