<?php
/**
 * Interface for ACL classes
 */

namespace Margay;

interface iACL {
    public function checkPermission($permission);
    public function redirectDefault();
}
