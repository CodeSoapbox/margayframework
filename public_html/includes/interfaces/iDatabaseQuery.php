<?php

/**
 * Interface for a database query class
 */
namespace Margay;

interface iDatabaseQuery {
    /**
     * returns the prepared query
     */
    public function getQuery();
    
    /**
     * Gets the fetch mode to be used
     */
    public function getFetchMode();
    
    /**
     * Sets the fetch mode to be used
     */
    public function setFetchMode($fetchMode);
    
    /**
     * Gets WHERE parameters
     */
    public function getParams();
    
    /**
     * Sets WHERE parameters
     */
    public function setParams(array $params);
}
