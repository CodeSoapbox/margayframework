<?php

/**
 *  Interface for a controller
 */

namespace Margay;

interface iController {
    public function getModel();
    public function getRouter();
    public function indexAction();
}
