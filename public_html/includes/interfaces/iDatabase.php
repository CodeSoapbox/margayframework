<?php

/**
 * Common interface for a database adapter
 **/
namespace Margay;

interface iDatabase {
    /**
     * Creates a database connection
     */
    public function connect();
    
    /**
     * Executes the query and returns the result.
     * @param \Margay\iDatabaseQuery $dbQuery
     */
    public function query(\Margay\iDatabaseQuery $dbQuery);
    
    /**
     * Sets the database host
     * @param type $host
     */
    public function setHost($host);
    
    /**
     * Sets the database user
     * @param type $user
     */
    public function setUser($user);
    
    /**
     * Sets the database password
     * @param type $pass
     */
    public function setPass($pass);
    
    /**
     * Sets the database name
     * @param type $dbName
     */
    public function setDbName($dbName);
}
