<?php

/* 
 * Common functionality
 */

namespace Margay;

$autoload_func = function($autoload_paths){
    $autoload = function ($class_name) use ($autoload_paths){
        //Get the actual file name (skipping namespace stuff)
        $parts = explode('\\', $class_name);
        $filename = end($parts) . '.php';
        foreach($autoload_paths as $root_path){
            //Using RecursiveDirectoryIterator to find all subdirectories
            //in path
            $iter = new \RecursiveIteratorIterator(
                    new \RecursiveDirectoryIterator(
                        //Using a global WEBSITE_DOCROOT variable here
                        //is bad form but for now I was not able to
                        //come up with a better solution
                        WEBSITE_DOCROOT.$root_path,
                        //Skip directories with single or double dots
                        \RecursiveDirectoryIterator::SKIP_DOTS
                    ),
                    //Parent element before children
                    \RecursiveIteratorIterator::SELF_FIRST,
                    //Ignore "Permission denied"
                    \RecursiveIteratorIterator::CATCH_GET_CHILD
            );
            
            $all_paths = array(
                $root_path
            );
            
            foreach($iter as $root_path => $dir){
                if($dir->isDir()){
                    $all_paths[] = $root_path;
                }
            }

            //Try to find the file in root directory and every subdirectory
            foreach($all_paths as $path){
                if(file_exists($path . '/' . $filename)){
                    require_once $path . '/' . $filename;
                    return true;
                }
            }
        }
        return false;
    };
    
    return $autoload;
};

spl_autoload_register(
    $autoload_func(
        array(
            'includes/classes',
            'includes/helpers',
            'includes/interfaces'
        )
    )
);