<?php

/*
 * Provides additional debugging functionality.
 */

/**
 * Description of Debug
 *
 * @author Dandi8
 */
namespace Margay;

class Debug {
    public static function print_r($expression){
        echo "<pre>";
        print_r($expression);
        echo "</pre>";
    }
}
