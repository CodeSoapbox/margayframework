<?php
/**
 * Represents a user
 */

namespace Margay;

class User implements iStorable{
    /**
     * @var iDatabase $database 
     */
    private $database;
    
    private $id;
    private $login;
    private $password;
    private $mail;
    private $name;
    private $dateRegistered;
    
    public function __construct(iDatabase $database) {
        $this->database = $database;
    }
    
    public function getId(){
        return $this->id;
    }
    
    public function setId($id){
        $this->id = $id;
    }
    
    public function getLogin(){
        return $this->login;
    }
    
    public function setLogin($login){
        $this->login = $login;
    }
    
    public function getPassword(){
        return $this->password;
    }
    
    public function setPassword($password){
        $this->password = $password;
    }
    
    public function getName(){
        return $this->name;
    }
    
    public function setName($name){
        $this->name = $name;
    }
    
    public function getDateRegistered(){
        return $this->dateRegistered;
    }
    
    public function setDateRegistered($dateRegistered){
        $this->dateRegistered = $dateRegistered;
    }


    public function save(){
        
    }
    
    public function load(){
        $dbQuery = new DatabaseQuerySelect(
                'users',
                array(
                    'id',
                    'login',
                    'pass',
                    'mail',
                    'name',
                    'date_registered'
                ),
                'id = ?'
        );
        
        $dbQuery->setParams(array($this->id));
                
        if($result = $this->database->query($dbQuery)){
            $result = $result->fetch();
            $this->id = $result['id'];
            $this->login = $result['login'];
            $this->password = $result['pass'];
            $this->mail = $result['mail'];
            $this->name = $result['name'];
            $this->dateRegistered = new \DateTime($result['date_registered']);
            
            return $this;
        }
        
        return false;
    }
}
