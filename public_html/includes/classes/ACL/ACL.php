<?php
/**
 * Access control list
 */

namespace Margay;

class ACL implements iACL{
    /**
     * @var int GUEST_ROLE_ID the id of the "guest" role in the database
     */
    const GUEST_ROLE_ID =  1;
    
    /**
     * @var iDatabase $database
     */
    private $database;
    
    /**
     * @var User $user
     */
    private $user;
    
    /**
     * @var array $permissions
     */
    private $permissions;
    
    /**
     * @var string $redirectURL URL to use when calling redirectDefault()
     */
    private $redirectURL;
    
    public function __construct(iDatabase $database) {
        $this->database = $database;
        
        $this->redirectURL = '/';
    }
    
    public function getUser(){
        return $this->user;
    }
    
    public function setUser(User $user){
        $this->user = $user;
    }
    
    private function getPermissions(){        
        if($this->permissions === null){
            $this->loadPermissions();
        }
        
        return $this->permissions;
    }
    
    private function loadPermissions(){
        $this->permissions = array();
        
        $params = null;

        if(isset($this->user)){
            $dbQuery = (new DatabaseQuerySelect(
                    'permissions', array(
                        'p.resource'
                    ),
                    'u.id = ?'
                )
            )
            ->setTableAlias('p')
            ->addJoin(new DatabaseQueryJoin('LEFT JOIN', 'role_grants', 'rg', 'rg.permission_id_fk = p.id'))
            ->addJoin(new DatabaseQueryJoin('LEFT JOIN', 'user_roles', 'ur', 'rg.role_id_fk = ur.id'))
            ->addJoin(new DatabaseQueryJoin('LEFT JOIN', 'role_members', 'rm', 'rm.user_role_id_fk = ur.id'))
            ->addJoin(new DatabaseQueryJoin('LEFT JOIN', 'users', 'u', 'rm.user_id_fk = u.id'));

            $params = array($this->user->getId()); //This should be the id of the current user
        } else {
            //For GUEST:
            $dbQuery = (new DatabaseQuerySelect(
                    'permissions AS p',
                    array(
                        'p.resource'
                    ),
                    'ur.id = ?'
            ))
            ->addJoin(new DatabaseQueryJoin('LEFT JOIN', 'role_grants', 'rg', 'rg.permission_id_fk = p.id'))
            ->addJoin(new DatabaseQueryJoin('LEFT JOIN', 'user_roles', 'ur', 'rg.role_id_fk = ur.id'))
            ->addJoin(new DatabaseQueryJoin('LEFT JOIN', 'role_members', 'rm', 'rm.user_role_id_fk = ur.id'));

            $params = array(self::GUEST_ROLE_ID);
        }
        
        $dbQuery->setParams($params);
        
        if($result = $this->database->query($dbQuery)){
            $result = $result->fetchAll();
            foreach($result as $permission){
                $this->permissions[] = $permission['resource'];
            }
        }
    }
    
    public function checkPermission($permission){
        return in_array(strtolower($permission), $this->getPermissions());
    }
    
    public function setRedirectURL($redirectURL){
        $this->redirectURL = $redirectURL;
    }
    
    public function redirectDefault() {
        header('Location: ' . $this->redirectURL);
    }
}
