<?php
namespace Margay;

/**
 * Dummy access control list class.
 * 
 * Always returns true.
 */
class DummyACL implements iACL{
    public function checkPermission($permission){
        return true;
    }
    
    public function redirectDefault() {}
}
