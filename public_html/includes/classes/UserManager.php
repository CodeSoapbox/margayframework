<?php
/**
 * Manages users
 */

namespace Margay;

class UserManager {
    /**
     * @var iDatabase $database 
     */
    private $database;
    
    private $loginAttemptFailed;
    
    public function __construct($database) {
        $this->database = $database;
        
        $this->loginAttemptFailed = false;
    }
    
    /**
     * Retrieves a user
     * @return User
     */
    public function getUser(){
        $userId = null;
        if(isset($_SESSION['userId'])){
            $userId = filter_var($_SESSION['userId'], FILTER_SANITIZE_NUMBER_INT);
        }
        
        //First try to load user from session
        if(isset($userId)){
            $user = new User($this->database);
            $user->setId($userId);
            if ($user->load()) {
                return $user;
            }
        }
        
        //Then try to load user from login action
        if($user = $this->handleLogin()){
            return $user;
        }
        
        return null;
    }
    
    private function handleLogin(){
        if (!empty($_POST)) {
            if (isset($_POST['login']) && isset($_POST['password'])) {
                $login = filter_input(INPUT_POST, 'login', FILTER_SANITIZE_STRING);
                $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
                
                //We shouldn't be doing any more operations on the login
                //and password so let's unset them from $_POST
                unset($_POST['login']);
                unset($_POST['password']);
                if($user = $this->loadUserByLoginAndPassword($login, $password)){
                    //If a redirect was requested with the login action,
                    //redirect the user to the desired page
                    if(isset($_POST['redirect'])){
                        $redirect = filter_input(INPUT_POST, 'redirect', FILTER_SANITIZE_URL);
                        header("Location: " . $redirect);
                        exit();
                    }
                    return $user;
                } else {
                    $this->loginAttemptFailed = true;
                }
            }
        }
        return false;
    }
    
    /**
     * Tries to load user with $login from the database.
     * 
     * If successful, verifies password. If the passwords match, returns
     * a User object.
     * @param type $login
     * @param type $password
     * @return \Margay\User|boolean
     */
    private function loadUserByLoginAndPassword($login, $password){
        $dbQuery = new DatabaseQuerySelect(
                'users',
                array(
                    'id',
                    'pass'
                ),
                'login = ?'
        );
        
        $dbQuery->setParams(array(
            $login
        ));
        
        if($result = $this->database->query($dbQuery)){
            $result = $result->fetch();
            
            if(isset($result)){
                //Check if passwords match
                if(password_verify($password, $result['pass'])){
                    $user = new User($this->database);
                    $user->setId($result['id']);
                    if($user->load()){
                        $_SESSION['userId'] = $user->getId();
                        return $user;
                    }
                }
            }
        }
        
        return false;
    }
    
    public function getLoginAttemptFailed(){
        return $this->loginAttemptFailed;
    }
    
    public function logout(){
        session_unset();
        session_destroy();
        
        header("Location: /");
        exit();
    }
}
