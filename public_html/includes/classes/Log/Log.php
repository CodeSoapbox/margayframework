<?php

/*
 * A basic Logging class.
 */
namespace Margay;

class Log{
    protected $options = array(
        'extension' => 'txt',
        'dateFormat' => 'Y-m-d G:i:s',
        'logName' => ''
    );

    public function __construct($logName = 'log'){
        $this->options['logName'] = $logName;
    }

    public function Log($message){
        if(!file_exists('log/')){
            mkdir('log', 0755);
        }
        file_put_contents('log/' . $this->getFileName(), '[' . $this->getTimestamp() . '] ' . $message . PHP_EOL, FILE_APPEND);
    }

    private function getFileName(){
        return $this->options['logName'] . '.' . $this->options['extension'];
    }

    private function getTimestamp() {
        return date($this->options['dateFormat']);
    }
}
