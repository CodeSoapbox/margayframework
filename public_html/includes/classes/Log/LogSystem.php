<?php

/*
 * Manages Log class instances.
 * 
 * At the moment implements a singleton anti-pattern for instances of loggers.
 * There are better ways to do it (e.g. dependency injection) but it was
 * decided that, in this case, the pros of using singletons outweigh the cons.
 */
namespace Margay;

class LogSystem{
    private static $instances = array();

    public static function getInstance($logger = null){
        if($logger == null){
            $logger = 'log';
        }
        if(!array_key_exists($logger, static::$instances)){
            static::$instances[$logger] = new Log($logger);
        }

        return static::$instances[$logger];
    }

    protected function __construct(){}
    protected function __clone(){}
}
