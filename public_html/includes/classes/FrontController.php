<?php

/*
 * The Front Controller class acts as a front controller for Margay.
 *
 * It provides a centralized entry point for handling requests
 * and in this case is responsible for creating and managing models,
 * views, controllers and basic logic related to them.
 */

namespace Margay;

class FrontController{
    private $model;
    private $controller;
    private $view;
    
    private $router;
    private $route;
    private $database;
    private $userManager;
    private $user;
    private $acl;
    private $action;

    public function __construct(Router $router, iDatabase $database, UserManager $userManager, User $user = null, iACL $acl){
        //Handle router stuff
        $router->handle();
        
        $this->router = $router;                
        $this->database = $database;
        $this->userManager = $userManager;
        $this->user = $user;
        $this->acl = $acl;
        
        //Get Route from route name
        $this->route = $router->getRoute();

        //Get action from URI
        $this->action = $router->getAction();

        //If a route exists, create a model for it.
        //Otherwise, change the route to an error before creating a new model.
        if ($this->routeExists()) {
            $this->createModel();
        } else {
            $this->redirectToError('pageNotFound');
        }

        //Check ACL for module permission
        if (!$this->acl->checkPermission('modules/' . $this->router->getModuleName() . '/access')) {
            die("You don't have permission to access this content!");
        }
        
        //Check ACL for action permission
        if (!$this->acl->checkPermission('modules/' . $this->router->getModuleName() . '/routes/' . $this->router->getRouteName() . '/' . $this->action)) {
            $this->acl->redirectDefault();
//            die("You don't have permission to call this action! <BR>"
//                    . 'modules/' . $this->router->getModuleName() . '/routes/' . $this->router->getRouteName() . '/' . $this->action);
        }

        //Create view and controller
        $this->createViewAndController();
        $this->view->setSiteTitle('Margay CMS');

        $this->callControllerAction();
    }
    
    /**
     * Check if a model, view and controller class exists for current route
     * @return boolean
     */
    private function routeExists(){
        $modelName = $this->route->getAnyModel();
        
        if (!class_exists($modelName) || !class_exists($this->route->getView()) || !class_exists($this->route->getController())) {
            //Log routing error
            $errorMsg = "A routing error occured while initializing Front Controller.";
            if (!class_exists($modelName)) {
                //Log lack of model
                $errorMsg .= PHP_EOL . "Model " . $modelName . " does not exist.";
            }
            if (!class_exists($this->route->getView())) {
                //Log lack of view
                $errorMsg .= PHP_EOL . "View " . $this->route->getView() . " does not exist.";
            }
            if (!class_exists($this->route->getController())) {
                //Log lack of controller
                $errorMsg .= PHP_EOL . "Controller " . $this->route->getController() . " does not exist.";
            }
            
            //Submit error log
            LogSystem::getInstance('exception')->Log($errorMsg);
            
            return false;
        }
        
        return true;
    }
    
    private function createModel(){
        $modelName = $this->route->getAnyModel();
        $this->model = new $modelName($this->database);
        $this->model->setUser($this->user);
        $this->model->setACL($this->acl);
        $this->model->setUserManager($this->userManager);
    }
    
    private function createViewAndController(){
        //Create view and controller
        $viewName = $this->route->getView();
        $controllerName = $this->route->getController();
        $this->view = new $viewName($this->model, $this->router);
        $this->controller = new $controllerName($this->model, $this->router);
    }
    
    private function redirectToError($action = 'index'){
        //Save current route as error route
        $errorRoute = $this->router->getRouteName();
        
        //Change route name to error
        $this->router->setRouteName('error');
        
        //Change route to error
        $this->route = $this->router->getRoute('error');
        
        //If the current module has no Error route, use the one from the 'base' 
        //module
        if(!$this->routeExists()){
            $this->router->setModuleName('base');
            $this->route = $this->router->getRoute('error');
        }
        
        //Create new model
        $this->createModel();
        $this->model->setErrorRoute($errorRoute);
        $this->action = $action;
    }
    
    private function callControllerAction(){
        //Execute controller action
        $this->action .=  'Action';
        if(method_exists($this->controller, $this->action)){
            $this->controller->{$this->action}();
        } else {
            //Log: call to unkown method
            LogSystem::getInstance('exception')->Log("Unknown action: " . $this->action . " was called within " . get_class($this->controller) . ".");
        }
    }

    /**
     * Outputs the rendered website
     * @return string
     */
    public function output(){
        return $this->view->output();
    }
}
