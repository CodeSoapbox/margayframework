<?php

/*
 * Model for the "About" page
 */
namespace Margay;

abstract class Model{
    private $database;
    private $user;
    private $userManager;
    private $acl;
    
    public function __construct($database) {
        $this->database = $database;
        
        //Do model things (whatever the child class needs to do in the
        //constructor)
        $this->init();
    }
    
    /**
     * Is executed when the class is instantiated.
     * 
     * Exists so that there is no need to override __construct() in children.
     */
    public function init(){}
    
    public function getDatabase(){
        return $this->database;
    }
    
    public function setDatabase($database){
        $this->database = $database;
    }
    
    public function getUser(){
        return $this->user;
    }
    
    public function setUser($user){
        $this->user = $user;
    }
    
    public function getUserManager(){
        return $this->userManager;
    }
    
    public function setUserManager($userManager){
        $this->userManager = $userManager;
    }
    
    public function getACL(){
        return $this->acl;
    }
    
    public function setACL($acl){
        $this->acl = $acl;
    }
}
