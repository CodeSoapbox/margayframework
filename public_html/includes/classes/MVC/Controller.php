<?php

/* 
 * Abstract class for controllers. Provides common functionality required for
 * usage with Margay.
 */

namespace Margay;

class Controller implements \Margay\iController{
    protected $model;
    protected $router;
    
    public function __construct($model, $router) {
        $this->model = $model;
        $this->router = $router;
    }
    
    public function getModel(){
        return $this->model;
    }
    
    public function getRouter(){
        return $this->router;
    }
    
    public function indexAction(){}
}