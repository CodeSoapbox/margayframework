<?php

/*
 * Abstract class for views. Provides common functionality required for
 * usage with Margay.
 */
namespace Margay;

abstract class View{
    protected $model;
    /**
     * @var Router $router
     */
    protected $router;

    private $siteTitle;
    private $pageTitle;

    private $templateName;
//    private $stylesheets = array();
//    private $javascripts = array();
//    private $body;

//    public $data = array();

    public function __construct($model, $router){
        $this->model = $model;
        $this->router = $router;

        $this->templateName = 'default';
    }

    public function getModel(){
        return $this->model;
    }

    public function getRouter(){
        return $this->router;
    }

    public function getSiteTitle(){
        return $this->siteTitle;
    }

    public function setSiteTitle($siteTitle){
        $this->siteTitle = $siteTitle;
    }

    public function getPageTitle(){
        return $this->pageTitle;
    }

    public function setPageTitle($pageTitle){
        $this->pageTitle = $pageTitle;
    }

    /**
     * Gets the full URL of the template.
     * 
     * If a $module name is given, will force the use of a URL for that module.
     * @param type $module (optional) the name of the module to use
     * @return string
     */
    public function getTemplateFullUrl($module = null){
        if($module == null){
            $module = $this->getRouter()->getModuleName();
        }
        return 'includes/modules/' . $module . '/templates/' . $this->templateName . '/';
    }

//    public function addCSS($path){
//        $this->stylesheets[] = $path;
//    }
//
//    public function getCSS(){
//        return $this->stylesheets;
//    }
//
//    public function addJavascript($path){
//        $this->javascripts[] = $path;
//    }
//
//    public function getJavascript(){
//        return $this->javascripts;
//    }
    
    /**
    * Outputs the rendered template
    * @return String
    */
    abstract public function output();

    function render($path){
        ob_start();
        //Retrieving the model variable for easier access in template
        $model = $this->getModel();
        include $path;
        return ob_get_clean();
    }
}
