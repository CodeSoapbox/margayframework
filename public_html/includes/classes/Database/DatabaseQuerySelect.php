<?php

/**
 * Represents a single database query.
 * 
 * Uses a fluent interface to build the query
 */

namespace Margay;

class DatabaseQuerySelect implements \Margay\iDatabaseQuery {
    private $table;
    private $tableAlias;
    private $columns;
    private $params;
    private $where;
    private $fetchMode;
    private $joins;
    
    public function __construct($table, Array $columns, $where = null) {
        $this->table = $table;
        $this->columns = $columns;
        $this->where = $where;
        $this->joins = array();
        
        $this->fetchMode = \PDO::FETCH_ASSOC;
    }
    
    public function setTableAlias($alias){
        $this->tableAlias = $alias;
        
        return $this;
    }
    
    public function addJoin(DatabaseQueryJoin $join){
        $this->joins[] = $join;
        
        return $this;
    }
    
    /**
     * Returns a prepared query
     * @return string
     */
    public function getQuery(){
        //Implode columns into a single string
        $columns_imploded = implode(', ', $this->columns);
        
        //Buld all JOINs into an array
        $join_strings = array();
        foreach($this->joins as $join){
            $join_strings[] = $join->build();
        }
        
        //Implode the array of joins into a single string
        $joins_imploded = '';
        if(!empty($this->joins)){
            $joins_imploded = ' ' . implode(' ', $join_strings);
        }
        
        $table = $this->table;
        
        if(isset($this->tableAlias)){
            $table .= " AS " . $this->tableAlias;
        }
        
        //Build the query
        $query = "SELECT $columns_imploded FROM $table" . $joins_imploded;
        
        if(isset($this->where)){
            $query .= " WHERE " . $this->where;
        }
        
        return $query;
    }
    
    /**
     * Returns query with parameters substituted for question marks
     * @return type
     */
    public function getQueryWithParams(){
        $query = $this->getQuery();
        $params = $this->getParams();
        
        foreach($params as $param){
            $query = implode($param, explode('?', $query, 2));
        }
        
        return $query;
    }
    
    public function getFetchMode(){
        return $this->fetchMode;
    }
    
    public function setFetchMode($fetchMode){
        $this->fetchMode = $fetchMode;
    }
    
    public function getParams(){
        return $this->params;
    }
    
    public function setParams(array $params){
        $this->params = $params;
    }
}
