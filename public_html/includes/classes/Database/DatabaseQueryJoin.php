<?php
/**
 * Represents a single join in a database query
 */

namespace Margay;

class DatabaseQueryJoin {
    private $joinType;
    private $table;
    private $tableAlias;
    private $joinCondition;
    
    public function __construct($joinType, $table, $tableAlias, $joinCondition) {
        $this->joinType = $joinType;
        $this->table = $table;
        $this->tableAlias = $tableAlias;
        $this->joinCondition = $joinCondition;
    }
    
    public function build(){
        return $this->joinType . " `" . $this->table . "` AS " . $this->tableAlias . " ON " . $this->joinCondition;
    }
}
