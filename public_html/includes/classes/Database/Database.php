<?php

/*
 * Class responsible for connecting and interacting with the database.
 */
namespace Margay;

class Database implements iDatabase{
    private $host;
    private $user;
    private $pass;
    private $dbName;
    /**
     *  @var $conn \PDO
     */
    private $conn;
    
    public function __construct() {
        
    }
    
    public function setHost($host){
        $this->host = $host;
        return $this;
    }
    
    public function setUser($user){
        $this->user = $user;
        return $this;
    }
    
    public function setPass($pass) {
        $this->pass = $pass;
        return $this;
    }
    
    public function setDbName($dbName) {
        $this->dbName = $dbName;
        return $this;
    }
    
    /**
     * Creates a database connection
     * @return boolean
     * @throws \LogicException
     */
    public function connect(){
        try{
            if(!isset($this->host)){
                throw new \LogicException("Cannot connect. Host not defined.");
            }
            //Check if we have everything we need to connect
            if(!isset($this->user)){
                throw new \LogicException("Cannot connect. User not defined.");
            }
            if(!isset($this->pass)){
                throw new \LogicException("Cannot connect. Password not defined.");
            }
            if(!isset($this->dbName)){
                throw new \LogicException("Cannot connect. Database name not defined.");
            }
        
            $this->conn = new \PDO("mysql:host=" . $this->host . ";dbname=" . $this->dbName, $this->user, $this->pass);
            //Set the PDO error mode to exception
            $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            return true;
        } catch (\Exception $ex) {
            LogSystem::getInstance('exception')->Log("Could not connect to database: " . $ex->getMessage());
            return false;
        }
    }
    
    /**
     * Executes the query and returns the result.
     * 
     * Will connect to the database automatically if a connection
     * is not already established.
     * @param \Margay\iDatabaseQuery $dbQuery
     * @return boolean
     */
    public function query(\Margay\iDatabaseQuery $dbQuery){
        if(isset($this->conn) || $this->connect()){
            $stmt = $this->conn->prepare($dbQuery->getQuery());
            $stmt->execute($dbQuery->getParams());
            $stmt->setFetchMode($dbQuery->getFetchMode());
            
            return $stmt;
        }
        return false;
    }
}
