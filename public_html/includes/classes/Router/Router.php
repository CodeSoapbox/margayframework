<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Margay;

class Router{
    private $moduleTable = array();
    private $routeTable = array();
    private $uri;
    private $moduleName;
    private $routeName;
    private $action;
    private $params;
    private $autoload_func;

    /**
     * @param \Margay\callable $autoload_func The autoloader function to use for including module classes
     */
    public function __construct(callable $autoload_func){
        $this->autoload_func = $autoload_func;
        $this->addModule('base');
        $this->moduleName = 'base';
        //Non-standard routes can be defined here (temporarily):
//        $this->routeTable['home'] = new Route('HomeModel', 'HomeView', 'HomeController');
//        $this->routeTable['other'] = new Route('OtherModel', 'OtherView', 'OtherController');
    }

    /**
     * Handles URL routing
     */
    public function handle(){
        //Get URI
        $this->uri = trim($_SERVER['REQUEST_URI'], '/');
        //Explode params from URI
        $params = explode('/', $this->uri);
        //Assign module name and route name
        $this->routeName = array_shift($params);
        
        //Fix for missing favicon request
        if($this->routeName == 'favicon.ico'){
            exit;
        }
        
        if($this->getModuleExists($this->routeName)){
            $this->moduleName = $this->routeName;
            $this->routeName = array_shift($params);
        }
        //Assign action
        $this->action = array_shift($params);
        if($this->action == null){
            $this->action = 'index';
        }
        //Assign extra parameters
        $this->params = $params;

        if(empty($this->routeName)){
            $this->routeName = 'home';
        }
    }

    /**
     * Checks if module $moduleName exists in the module table
     * @param  String $moduleName
     * @return Bool
     */
    public function getModuleExists($moduleName){
        if(in_array($moduleName, $this->moduleTable)){
            return true;
        }
        return false;
    }

    /**
     * Adds module $moduleName to the module table and registers relevant
     * module folders in the autoloader
     * @param String $moduleName
     */
    public function addModule($moduleName){
        $this->moduleTable[] = $moduleName;
        
        //Register additional module folders in autoloader
        spl_autoload_register(
                $this->autoload_func->__invoke(
                        array(
                            'includes/modules/' . $moduleName . '/models',
                            'includes/modules/' . $moduleName . '/views',
                            'includes/modules/' . $moduleName . '/controllers',
                        )
                )
        );
    }
    
    public function getModuleName(){
        return $this->moduleName;
    }

    /**
     * Sets the current module name to $moduleName
     * @param String $moduleName
     */
    public function setModuleName($moduleName){
        $this->moduleName = $moduleName;
    }

    /**
     * Returns MVC route information
     * @param String $route
     * @return \Route
     */
    public function getRoute($routeName = null){
        if($routeName == null){
            $routeName = $this->routeName;
        }

        $routeName = strtolower($routeName);

        if(!empty($this->routeTable[$routeName])){
            return $this->routeTable[$routeName];
        }

        $moduleName = empty($this->moduleName) ? null : "\\" . $this->moduleName;

        return new Route($routeName.'Model', $routeName.'View', $routeName.'Controller', __NAMESPACE__. $moduleName);
    }
    
    public function getRouteName(){
        return $this->routeName;
    }
    
    public function setRouteName($routeName){
        $this->routeName = $routeName;
    }

    public function getAction(){
        return $this->action;
    }

    public function setAction($action){
        $this->action = $action;
    }

    public function getParams(){
        return $this->params;
    }

    public function getParam($index){
        return $this->params[$index];
    }
}
