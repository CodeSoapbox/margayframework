<?php

/*
 * Holds information regarding a single route.
 */
namespace Margay;

class Route{
    private $model;
    private $view;
    private $controller;
    private $namespace;

    public function __construct($model, $view, $controller, $namespace = null){
        $this->model = $model;
        $this->view = $view;
        $this->controller = $controller;
        $this->namespace = $namespace;
    }

    public function getModel(){
        $namespace = isset($this->namespace) ? $this->namespace . '\\' : null;
        return $namespace . $this->model;
    }

    public function getRawModel(){
        return __NAMESPACE__ . '\\' . $this->model;
    }

    public function getAnyModel(){
        $model = $this->getModel();
        if(class_exists($model)){
            return $model;
        }
        return $this->getRawModel();
    }

    public function getView(){
        $namespace = isset($this->namespace) ? $this->namespace . '\\' : null;
        return $namespace . $this->view;
    }

    public function getController(){
        $namespace = isset($this->namespace) ? $this->namespace . '\\' : null;
        return $namespace . $this->controller;
    }
}
