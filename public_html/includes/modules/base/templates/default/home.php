<?php include 'header.php'; ?>
                <div class="center">    
                    <ul class="pagination">
                        <li><a href="#">«</a></li>
                        <li><a href="#" class="active">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li class="empty">...</li>
                        <li><a href="#">21</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                    
                    <p><a href="/<?php echo $this->getRouter()->getRouteName(); ?>/textclicked"><?php echo $this->model->getText();?></a></p>
                </div>
                <?php
                $posts = $this->model->getPosts();

                if(empty($posts)):
                ?>
                <p style="text-align: center;">No posts were found. You should try uploading some images!</p>
                <?php
                else:
                foreach($posts as $post):
                ?>
                <div class="post">
                    <div class="post-datetime">
                        <p class="post-date">28.05.16</p>
                        <p class="post-time">23:39</p>
                    </div>
                    <div class="post-image-wrapper">
                        <div class="image-info-wrapper">
                            <div class="image-info">
                                <p>Click to enlarge (not yet implemented)</p>
                            </div>
                        </div>
                        <img src="<?php echo $post->getImage(); ?>" class="post-image">
                    </div>
                    <p class="post-title"><a href="/post/view/<?php echo $post->getId(); ?>"><?php echo $post->getTitle(); ?></a></p>
                    <p class="post-description"><?php $post->getDescription(); ?></p>
                    <p class="read-more"><a href="/post/view/<?php echo $post->getId(); ?>">Read more...</a></p>
                </div>
                <?php
                    endforeach;
                endif;
                ?>
                <div class="center">
                    <ul class="pagination">
                        <li><a href="#">«</a></li>
                        <li><a href="#" class="active">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li class="empty">...</li>
                        <li><a href="#">21</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                </div>
                <?php //echo $this->getBody(); ?>
<?php include 'footer.php'; ?>