<?php include 'header.php'; ?>

<?php
$post = $model->getPost();
if(!empty($post)):
?>
<div class="post">
    <div class="post-datetime">
        <p class="post-date">28.05.16</p>
        <p class="post-time">23:39</p>
    </div>
    <div class="post-image-wrapper">
        <div class="image-info-wrapper">
            <div class="image-info">
                <p>Click to enlarge (not yet implemented)</p>
            </div>
        </div>
        <img src="<?php echo $post->getImage(); ?>" class="post-image">
    </div>
    <p class="post-title"><?php echo $post->getTitle(); ?></p>
    <p class="post-description"><?php echo $post->getDescription(); ?></p>
</div>
<?php
else:
?>
Post not found.
<?php
endif;
?>

<?php include 'footer.php'; ?>