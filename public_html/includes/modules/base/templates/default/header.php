<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $this->getSiteTitle(); ?> - <?php echo $this->getPageTitle(); ?></title>
        <link href='https://fonts.googleapis.com/css?family=Lato:400,900|Open+Sans:400,700|Open+Sans+Condensed:300,700&subset=latin-ext' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/<?php echo $this->getTemplateFullURL(); ?>css/style.css">
        <meta charset="utf-8">
    </head>

    <body>
        <div id="main-wrapper">
            <div id="header">
                <h1><?php echo $this->getSiteTitle(); ?></h1>
            </div>
            <div id="main">

                <div class="center">
                    <ul class="menu">
                        <li><a href="/">HOME</a></li>
                        <li><a href="/about">ABOUT</a></li>
                    </ul>
                </div>
