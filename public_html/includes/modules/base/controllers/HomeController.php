<?php

/*
 * Home page controller.
 */
namespace Margay\Base;

class HomeController extends \Margay\Controller{
    public function __construct(\Margay\Base\HomeModel $model, \Margay\Router $router) {
        parent::__construct($model, $router);
    }
    
    public function indexAction(){
        $this->getModel()->setText("This text was changed because the default action was called.");
    }

    /**
     * Shows the page with the sample text changed (for testing purposes)
     */
    public function textClickedAction(){
        $this->model->setText("Text updated");
    }

    /**
     * Lists URL parameters as interpreted by the router (for testing purposes)
     */
    public function testAction(){
        echo "<p>Testing...</p>";
        $router = $this->getRouter();
        Debug::print_r($router->getParams());
    }
}
