<?php

/*
 * Error page controller.
 */
namespace Margay\Base;

class ErrorController extends \Margay\Controller{
    public function __construct(\Margay\Base\ErrorModel $model, \Margay\Router $router){
        parent::__construct($model, $router);
    }

    public function pageNotFoundAction(){
        $this->model->setMessage("<p>Error 404!</p> <p>The page '" . $this->model->getErrorRoute() . "' could not be found.</p>");
    }
}
