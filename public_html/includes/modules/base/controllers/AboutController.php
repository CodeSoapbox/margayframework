<?php

/*
 * "About" page controller.
 */
namespace Margay\Base;

class AboutController extends \Margay\Controller{
    public function __construct(\Margay\Base\AboutModel $model, \Margay\Router $router) {
        parent::__construct($model, $router);
    }
}
