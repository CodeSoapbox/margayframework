<?php

/*
 * Single post page controller.
 */
namespace Margay\Base;

class PostController extends \Margay\Controller{

    public function __construct(\Margay\Base\PostModel $model, \Margay\Router $router) {
        parent::__construct($model, $router);
    }
    
    public function viewAction(){
        $postId = $this->getRouter()->getParams()[0];
        $this->getModel()->setPost($postId);
    }
}
