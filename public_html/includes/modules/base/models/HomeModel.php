<?php

/*
 * Model for the home page
 */
namespace Margay\Base;

class HomeModel extends \Margay\Model{
    private $text;
    
    public function init(){
        $this->text = "Click me and I will change (I'm here for testing purposes)!";
    }

    /**
     * Gets the sample text (for testing purposes)
     * @return string
     */
    public function getText(){
        return $this->text;
    }

    /**
     * Sets the sample text (for testing purposes)
     * @param string $text
     */
    public function setText($text){
        $this->text = $text;
    }

    /**
     * Retrieves a list of posts from the database and returns them
     * as an array of objects.
     * @return array \Margay\Post
     */
    public function getPosts(){
        /* @var $db iDatabase */
        $db = $this->getDatabase();
        $query = new \Margay\DatabaseQuerySelect("posts", array('id', 'title', 'image', 'description', 'user_id_fk', 'date_created', 'date_modified'));
        $result = $db->query($query)->fetchAll();
        
        $posts = array();
        if(!empty($result)){
            foreach($result as $postData){
                $post = new Post($this->getDatabase());
                $post->setId($postData['id']);
                $post->setTitle($postData['title']);
                $post->setImage($postData['image']);
                $post->setDescription($postData['description']);

                $posts[] = $post;
            }
        }
        
        return $posts;
    }

    /**
     * Returns random images from Flickr using $topic as search term
     * @param string $topic
     * @return string
     */
//    function getRandomImages($topic = '') {
//        $url = 'https://api.flickr.com/services/rest/?&method=flickr.photos.search&api_key=5fd59bceffd6299037e7e497c69e6dab&sort=relevance&format=json&nojsoncallback=1&text=' . $topic;
//        $url_contents = file_get_contents($url);
//        $photos_info = null;
//        if(sizeof($url_contents>0)){
//            $photos_info = json_decode($url_contents)->photos->photo;
//        }
//        $photos = array();
//        if(is_array($photos_info)){
//            foreach($photos_info as $photo){
//                $photos[] = 'https://farm' . $photo->farm . '.staticflickr.com/' . $photo->server . '/' . $photo->id . '_' . $photo->secret . '.jpg';
//            }
//        }
//        return $photos;
//    }
}
