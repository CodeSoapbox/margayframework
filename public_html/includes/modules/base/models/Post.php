<?php

/*
 * Domain model for a post.
 */
namespace Margay\Base;

class Post implements \Margay\iStorable{
    /**
     * @var iDatabase $database 
     */
    private $database;
    
    private $id;
    private $title;
    private $image;
    private $description;    
    private $userId;
    private $dateCreated;
    private $dateModified;

    public function __construct($database) {
        $this->database = $database;
    }

    public function getId(){
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
    }

    public function getTitle(){
        return $this->title;
    }
    
    public function setTitle($title){
        $this->title = $title;
    }

    public function getImage(){
        return $this->image;
    }
    
    public function setImage($image){
        $this->image = $image;
    }

    public function getDescription(){
        return $this->description;
    }
    
    public function setDescription($description){
        $this->description = $description;
    }
    
    public function getUserId(){
        return $this->userId;
    }
    
    public function setUserId($userId){
        $this->userId = $userId;
    }
    
    public function getDateCreated(){
        return $this->dateCreated;
    }
    
    public function setDateCreated($dateCreated){
        $this->dateCreated = $dateCreated;
    }
    
    public function getDateModified(){
        return $this->dateModified;
    }
    
    public function setDateModified($dateModified){
        $this->dateModified = $dateModified;
    }
    
    public function load(){
        $dbQuery = new \Margay\DatabaseQuerySelect(
                'posts',
                array(
                    'id',
                    'title',
                    'image',
                    'description',
                    'user_id_fk',
                    'date_created',
                    'date_modified'
                ),
                'id = ?'
        );
        
        $dbQuery->setParams(array($this->id));
        
        if($result = $this->database->query($dbQuery)){
            $result = $result->fetch();
            $this->id = $result['id'];
            $this->title = $result['title'];
            $this->image = $result['image'];
            $this->description = $result['description'];
            $this->userId = $result['user_id_fk'];
            $this->dateCreated = $result['date_created'];
            $this->dateModified = $result['date_modified'];
            
            return $this;
        }
        
        return false;        
    }
    
    public function save(){
        
    }
}
