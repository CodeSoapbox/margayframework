<?php

/*
 * Model for handling errors
 */
namespace Margay\Base;

class ErrorModel extends \Margay\Model{
    private $errorRoute;
    private $message;

    public function setErrorRoute($route){
        $this->errorRoute = $route;
    }

    public function getErrorRoute(){
        return $this->errorRoute;
    }

    public function getMessage(){
        return $this->message;
    }

    public function setMessage($message){
        $this->message = $message;
    }
}
