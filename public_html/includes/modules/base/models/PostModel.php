<?php

/*
 * Model for a single post page.
 */
namespace Margay\Base;

class PostModel extends \Margay\Model{
    /* @var $post \Margay\Post */
    protected $post;
    
    /**
     * @return \Margay\Post
     */
    public function getPost(){
        return $this->post;
    }
    
    /**
     * Retrieves a post with the given ID from the DB.
     * 
     * At the moment only creates a sample post.
     * @param string $id The id of the post in the database
     * @return \Margay\Post
     */
    public function setPost($id){
        $this->post = new Post($this->getDatabase());
        $this->post->setId($id);
        $this->post = $this->post->load();
        return $this->post;
    }
}
