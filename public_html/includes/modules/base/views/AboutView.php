<?php

/*
 * "About" page view.
 */
namespace Margay\Base;

class AboutView extends \Margay\View{
    /**
     * Outputs the rendered template
     * @return String
     */
    public function output(){
        $this->setPageTitle("About");
        return $this->render($this->getTemplateFullUrl() . 'about.php');
    }
}
