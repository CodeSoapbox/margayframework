<?php

/*
 * Home page view (post list).
 */
namespace Margay\Base;

class HomeView extends \Margay\View{
//        private $model;
//        public $route;
//        public $data = array();

//        public function __construct($route, HomeModel $model) {
//            $this->model = $model;
//            $this->route = $route;
//        }

    /**
     * Outputs the rendered template
     * @return String
     */
    public function output(){
        $this->setPageTitle("Home");
        return $this->render($this->getTemplateFullUrl() . 'home.php');
    }
}
