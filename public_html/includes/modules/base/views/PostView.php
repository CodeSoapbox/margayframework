<?php

/*
 * View for a single post.
 */
namespace Margay\Base;

class PostView extends \Margay\View{
    /**
     * Outputs the rendered template
     * @return String
     */
    public function output(){
        $this->setPageTitle("Single post view");
        return $this->render($this->getTemplateFullUrl() . 'singlePost.php');
    }
}
