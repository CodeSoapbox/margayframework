<?php

/*
 * Error page view.
 */
namespace Margay\Base;

class ErrorView extends \Margay\View{
    public function output(){
    $this->setPageTitle("Error!");
    return $this->render($this->getTemplateFullUrl() . 'error.php');
    }
}
