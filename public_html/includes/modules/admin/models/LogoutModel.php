<?php

/*
 * Model for the admin area login page
 */
namespace Margay\Admin;

class LogoutModel extends \Margay\Model{
    public function logout(){
        $this->getUserManager()->logout();
    }
}
