<?php

/*
 * Admin area login page view.
 *
 * For testing purposes only.
 */

namespace Margay\Admin;

use Margay\View;

class LogoutView extends View{
    /**
     * Outputs the rendered template
     * @return String
     */
    public function output(){
        $this->setPageTitle("Log out");
        return null;
    }
}
