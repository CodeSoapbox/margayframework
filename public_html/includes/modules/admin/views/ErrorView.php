<?php

/*
 * Admin area error page view.
 */
namespace Margay\Admin;

use Margay\View;

class ErrorView extends View{
    public function output(){
    $this->setPageTitle("Error!");
    return $this->render($this->getTemplateFullUrl() . 'error.php');
    }
}
