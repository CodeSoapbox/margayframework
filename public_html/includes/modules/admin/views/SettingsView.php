<?php

/*
 * Admin area "Settings" page view.
 *
 * For testing purposes only.
 */

namespace Margay\Admin;

use Margay\View;

class SettingsView extends View{
    /**
     * Outputs the rendered template
     * @return String
     */
    public function output(){
        $this->setPageTitle("Settings");
        return $this->render('includes/modules/admin/templates/default/settings.php');
    }
}
