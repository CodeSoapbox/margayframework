<?php

/*
 * Admin area "Dashboard" page view.
 *
 * For testing purposes only.
 */

namespace Margay\Admin;

use Margay\View;

class HomeView extends View{
    /**
     * Outputs the rendered template
     * @return String
     */
    public function output(){
        $this->setPageTitle("Dashboard");
        return $this->render('includes/modules/admin/templates/default/home.php');
    }
}
