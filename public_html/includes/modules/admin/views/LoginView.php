<?php

/*
 * Admin area login page view.
 *
 * For testing purposes only.
 */

namespace Margay\Admin;

use Margay\View;

class LoginView extends View{
    /**
     * Outputs the rendered template
     * @return String
     */
    public function output(){
        $this->setPageTitle("Log in");
        return $this->render('includes/modules/admin/templates/default/login.php');
    }
}
