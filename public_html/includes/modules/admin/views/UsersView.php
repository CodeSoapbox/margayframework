<?php

/*
 * Admin area "Users" page view.
 *
 * For testing purposes only.
 */

namespace Margay\Admin;

use Margay\View;

class UsersView extends View{
    /**
     * Outputs the rendered template
     * @return String
     */
    public function output(){
        $this->setPageTitle("Users");
        return $this->render('includes/modules/admin/templates/default/usersList.php');
    }
}
