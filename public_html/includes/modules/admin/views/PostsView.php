<?php

/*
 * Admin area "Posts" page view.
 *
 * For testing purposes only.
 */

namespace Margay\Admin;

use Margay\View;

class PostsView extends View{
    /**
     * Outputs the rendered template
     * @return String
     */
    public function output(){
        $this->setPageTitle("Posts");
        return $this->render('includes/modules/admin/templates/default/postsList.php');
    }
}
