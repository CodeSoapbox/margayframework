<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $this->getSiteTitle(); ?> - <?php echo $this->getPageTitle(); ?></title>
        <link href='https://fonts.googleapis.com/css?family=Lato:400,900|Open+Sans:400,700|Open+Sans+Condensed:300,700&subset=latin-ext' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/<?php echo $this->getTemplateFullURL(); ?>css/style.css">
        <meta charset="utf-8">
    </head>

    <body>
        <div id="header">
            MARGAY CMS - ADMINISTRATION AREA
        </div>
        <div id="login">
            <?php if($model->getUser() == null): ?>
            <h1>Log in</h1>
            <p>Enter your credentials, mortal.</p>
            <?php if($model->getUserManager()->getLoginAttemptFailed()): ?>
            <p>Wrong username or password! Try again.</p>
            <?php endif; ?>

            <form method="post" action="">
                <p><label for="login">Login: </label><input type="text" name="login"></p>
                <p><label for="login">Password: </label><input type="password" name="password"></p>
                <p><input type="submit" value="Log in"></p>
                <input type="hidden" name="redirect" value="/admin">
            </form>
            <?php else: ?>
                You are already logged in.
            <?php endif; ?>
        </div>
    </body>
</html>