<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $this->getSiteTitle(); ?> - <?php echo $this->getPageTitle(); ?></title>
        <link href='https://fonts.googleapis.com/css?family=Lato:400,900|Open+Sans:400,700|Open+Sans+Condensed:300,700&subset=latin-ext' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/<?php echo $this->getTemplateFullURL(); ?>css/style.css">
        <meta charset="utf-8">
    </head>

    <body>
        <div id="header">
            MARGAY CMS - ADMINISTRATION AREA
            <a href="/admin/logout" id='logout-button'>Log out</a>
            <!--<h1>TEST</h1>-->
        </div>
        
        <div id="sidebar">
            <!--<div>-->
            <ul class="sidebar-menu">
                <li><a href="/admin/">DASHBOARD</a></li>
                <li><a href="/admin/posts">POSTS</a></li>
                <li><a href="/admin/settings">SETTINGS</a></li>
                <li><a href="/admin/users">USERS</a></li>
            </ul>
            <!--</div>-->
        </div>
        <div id="content">