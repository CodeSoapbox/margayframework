<?php

/*
 * Controller for the admin area dashboard page.
 */

namespace Margay\Admin;

class HomeController extends \Margay\Controller{
    public function __construct(\Margay\Admin\HomeModel $model, \Margay\Router $router) {
        parent::__construct($model, $router);
    }
}
