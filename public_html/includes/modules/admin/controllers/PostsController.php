<?php

/*
 * Controller for the admin area dashboard page.
 */

namespace Margay\Admin;

class PostsController extends \Margay\Controller{
    public function __construct(PostsModel $model, \Margay\Router $router) {
        parent::__construct($model, $router);
    }
}
