<?php

/*
 * Controller for the admin area dashboard page.
 */

namespace Margay\Admin;

class UsersController extends \Margay\Controller{
    public function __construct(UsersModel $model, \Margay\Router $router) {
        parent::__construct($model, $router);
    }
}
