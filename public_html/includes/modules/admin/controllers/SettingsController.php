<?php

/*
 * Controller for the admin area dashboard page.
 */

namespace Margay\Admin;

class SettingsController extends \Margay\Controller{
    public function __construct(SettingsModel $model, \Margay\Router $router) {
        parent::__construct($model, $router);
    }
}
