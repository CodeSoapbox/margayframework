<?php

/*
 * Controller for the admin area logout page.
 */

namespace Margay\Admin;

class LogoutController extends \Margay\Controller{
    public function __construct(LogoutModel $model, \Margay\Router $router) {
        parent::__construct($model, $router);
    }
    
    public function indexAction(){
        $this->getModel()->logout();
    }
}
